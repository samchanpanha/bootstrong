/*Instructions 

Script helps to convert a csv with Facebook URLs to Unique Facebook IDs 

Input

node fb.js data.csv

Sample csv 
url
https://facebook.com/9gag/
https://facebook.com/shashitechno

Output

https://facebook.com/9gag/,21785951839
https://facebook.com/shashitechno,100000881971724

*/

var request = require("sync-request"); // sync request so we can write the result into file synchronously
var fs = require("fs"); // FS module to deal with filesytem i.e reading the csv file.

var API_URL = "https://findmyfbid.in/apiv1/"; // Find MY FB ID API URL

var Token = "{token}"; // Get the token from FIND MY FB ID https://findmyfbid.in/developer-api/

let csv = require("fast-csv"); // fast csv to read and write into csv file.

var fast_csv = csv.createWriteStream(); // get the writ stream
var writeStream = fs.createWriteStream("output.csv"); // write the results into output.csv
fast_csv.pipe(writeStream);

var inputFile = process.argv.slice(2)[0]; // input file is taken from CMD input for example "node fb.js data.csv"

let csvstream = csv
    .fromPath(inputFile, { headers: false })
    .on("data", function(row) {
        csvstream.pause();
        if (row[0] != "url") {
            // skip the header
            getFacebookID(row[0], function(err, result) {});
        }
        csvstream.resume();
    })
    .on("end", function() {
        console.log("We are done!");
    })
    .on("error", function(error) {
        console.log(error);
    });

function writeData(url, code) {
    fast_csv.write([url, code]);
}

function getFacebookID(url) {
    console.log("processing " + url);
    var res = request("POST", API_URL, {
        headers: {
            Authorization: "Token " + Token,
        },
        json: {
            fburl: url,
        },
    });
    var result = JSON.parse(res.getBody("utf8"));
    writeData(url, result["code"]); // get the code from result output.
}