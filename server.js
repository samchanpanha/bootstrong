//use

var express = require("express");
var cors = require("cors");
const axios = require("axios");
var app = express();
const path = require("path");
const router = express.Router();
const bodyParser = require("body-parser");
var mysql = require("mysql");
const fs = require("fs");

const cheerio = require("cheerio");

require("dotenv").config();

// add middleware
app.use(express.static("public"));
app.use(bodyParser.json());

// parse requests of content-type: application/x-www-form-urlencoded
// Make sure you place body-parser before your CRUD handlers!
app.use(bodyParser.urlencoded({ extended: true }));

// app.use(cors());
router.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");

    // authorized headers for preflight requests
    // https://developer.mozilla.org/en-US/docs/Glossary/preflight_request
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept"
    );
    next();

    router.options("*", (req, res) => {
        // allowed XHR methods
        res.header(
            "Access-Control-Allow-Methods",
            "GET, PATCH, PUT, POST, DELETE, OPTIONS"
        );
        res.send();
    });
});

const findDuplicates = (arr) => {
    let sorted_arr = arr.slice().sort(); // You can define the comparing function here.
    // JS by default uses a crappy string compare.
    // (we use slice to clone the array so the
    // original array won't be modified)
    let results = [];
    for (let i = 0; i < sorted_arr.length - 1; i++) {
        if (sorted_arr[i + 1] == sorted_arr[i]) {
            results.push(sorted_arr[i]);
        }
    }
    return results;
};

function arrayReplace(text, arrFrom, arrTo, caseSensitive) {
    return text
        .replace(/</g, " <<")
        .replace(/>/g, ">> ")
        .replace(/([\.\;\,])/g, " <$1> ")
        .split(" ")
        .map(function(value) {
            var pos = arrFrom.indexOf(caseSensitive ? value : value.toLowerCase());
            if (pos == -1) {
                return value;
            } else {
                return arrTo[pos % arrTo.length];
            }
        })
        .join(" ")
        .replace(/( \<|\> )/g, "");
}

const getProfileUserIds = async(url) => {
    const response = await axios.get(url);
    const $ = cheerio.load(response.data);
    try {
        if ($.html().toString().includes("userID")) {
            return await haveUserId($); // if userID is in the html
        } else return await haveEntityId($); // if entity_id is in the html
    } catch (error) {
        console.log(url);
        console.log(error);
    }
};

const haveUserId = (html) => {
    try {
        const data = html.html().toString().split(",");
        let finalUserID = [...new Set(data)]
            .join(",")
            .replace(/\"userID\"/g, `'userID'`)
            .replace(/\"/g, `'`)
            .split(",")
            .filter((x) => x.includes("userID"))[0]
            .split(":")[1]
            .replace(/'/g, "");
        return finalUserID;
    } catch (error) {
        console.log(error);
    }
};

const haveEntityId = (html) => {
    try {
        let finalEntityId = [...new Set(html.html().toString().split("&amp;"))]
            .filter((x) => x.includes("entity_id="))
            .filter((x) => x.split("").length < 30)[0]
            .replace(/entity_id=/g, "");
        return finalEntityId;
    } catch (error) {
        console.log(error);
    }
};

function countWords(str) {
    var matches = str.match(/[\w\d\’\'-]+/gi);
    return matches ? matches : 0;
}

router.get("/", function(req, res) {
    res.sendFile(path.join(__dirname + "/public/index.html"));
    //__dirname : It will resolve to your project folder.
});

router.get("/scrape", function(req, res) {
    res.sendFile(path.join(__dirname + "/public/scrape.html"));
    //__dirname : It will resolve to your project folder.
});

router.post("/api/v1/facebook-comments", async function(req, res, next) {
    try {
        const { token } = req.body;
        const data = await fs.readFileSync(__dirname + "/b.txt", "utf8");
        const $ = cheerio.load(data.toString());
        const urlElements = $(`${process.env.BLOCK_ARTICLES}`);
        const comments = [];
        const day = [];
        const numberPattern = /[^\d\W]+$/g;
        let userData = [];
        let fb = [];

        urlElements.each(async(i, el) => {
            const urlUser = $(el).find("a").attr("href");
            if (
                urlUser != null &&
                !urlUser.includes("https://www.facebook.com/ganzberg.beer/?")
            ) {
                const text = $(el).find(`${process.env.BLOCK_TEXTS}`).text().toString();
                if (
                    $(el)
                    .find("a")
                    .attr("href")
                    .includes("https://www.facebook.com/photo.php")
                ) {
                    fb.push(urlUser);
                } else {
                    if (urlUser.includes("https://www.facebook.com/profile.php")) {
                        userData.push({
                            id: i,
                            profile: urlUser.split("&")[0],
                            name: "",
                            comments: text,
                            account: urlUser
                                .split("?")[1]
                                .split("&")[0]
                                .toString()
                                .replace(/[^0-9]/g, ""),
                            link_to_comments: urlUser,
                            image: "",
                        });
                    } else {
                        userData.push({
                            id: i,
                            profile: "",
                            name: urlUser.split("?")[0],
                            comments: text,
                            account: "",
                            link_to_comments: urlUser,
                            image: "",
                        });
                    }
                }
            }
        });
        // get image user : https://graph.facebook.com/v9.0/100003630629228/picture?type=large&redirect=false&access_token=
        userData = await getUniqueListBy(userData, "comments");
        let accountUserId = await getAccountId(userData);
        for (let user of userData) {
            const id =
                user.account !== "" ?
                user.account :
                accountUserId.filter((x) => x.name === user.name)[0].account;
            if (id !== "") {
                const url = await `https://graph.facebook.com/v9.0/${id}/picture?type=large&redirect=false&access_token=${token}`;
                const response = await axios.get(url);
                const {data} = await response.data;
                user.image = data.url;
                if (user.account === "") {
                    user.account = id;
                    // console.log(id);
                }
            }
        }

        console.log(accountUserId)

        res.json({
            userData: userData,
        });
        // res.json(req.body);
        // res.sendFile(path.join(__dirname + "/b.txt"));
    } catch (error) {
        console.error(error);
        throw error;
    }
    //__dirname : It will resolve to your project folder.
});

function getUniqueListBy(arr, key) {
    return [...new Map(arr.map((item) => [item[key], item])).values()];
}

const getAccountId = async(urls) => {
    try {
        let arrURL = getUniqueListBy(urls, "comments");
        arrURL = getUniqueListBy(arrURL, "name");
        const show =
            "?show_switched_toast=0&show_switched_tooltip=0&show_podcast_settings=0";
        let allID = [];
        let results = [];
        for (let i = 0; i < arrURL.length; i++) {
            const getUrl = arrURL[i].name;
            const getId = arrURL[i].id;
            const comment = arrURL[i].comments;
            if (getUrl != "" && comment != "") {
                let url = `${getUrl}${show}`;
                let { data } = await axios.get(`${url}`);
                const checkDuplicate = await results.filter((x) => x.url.includes(url));
                if (checkDuplicate.length === 0) {
                    console.log(url);
                    results.push({
                        id: getId,
                        data: data,
                        url: url,
                    });
                }
            }
        }

        for (let i = 0; i < results.length; i++) {
            let getResultId = await results[i].id;
            let convertData = [];
            // convert array to string

            let convertStrings = "";

            convertData = await results[i].data
                .split(",")
                .filter((x) => x.includes("userID"));
            if (convertData.length > 0) {
                try {
                    convertStrings = await convertData[0].toString().replace(/[^0-9]/g, "");
                } catch (error) {}

                allID.push({
                    id: getResultId,
                    account: convertStrings,
                    name: results[i].url.split("?")[0],
                });
            } else {
                let getResult = await results[i].data
                    .split(",")
                    .filter((x) => x.includes('"entity_id"'));
                if (getResult.length > 0) {
                    allID.push({
                        id: getResultId,
                        account: getResult.toString().replace(/[^0-9]/g, ""),
                        name: results[i].url.split("?")[0],
                    });
                } else {
                    allID.push({
                        id: getResultId,
                        account: convertStrings,
                        name: results[i].url.split("?")[0],
                    });
                }
            }
        }

        return allID;
    } catch (error) {
        console.log("not work with url = ", error);
    }
};

router.get("/about", function(req, res) {
    res.sendFile(path.join(__dirname + "/about.html"));
});

router.get("/sitemap", function(req, res) {
    res.sendFile(path.join(__dirname + "/sitemap.html"));
});

router.post("/is-me/:id/:token", async(req, res) => {
    try {
        const id = req.params.id,
            token = req.params.token;
        // console.log(req);

        const url = `
    https: //graph.facebook.com/v9.0/${id}/comments?fields=from,message,created_time,id&limit=1000&access_token=${token}`;
        const { data } = await axios({
            url: url,
            method: "get",
            headers: {
                "Content-Type": "application/json",
            },
        });

        res.json(data);
    } catch (error) {
        console.error(error);
        throw error;
    }
});

router.post("/api/page/:id/:token", async(req, res) => {
    try {
        const id = req.params.id;
        const token = req.params.token;
        //         const url = `https://graph.facebook.com/v9.0/${id}/comments?fields=message,from,created_time,updated_time,
        // comments.limit(0).summary(1),shares,reactions.limit(0).summary(1),reactions.type(LIKE).limit(0).summary(1).as(like),reactions.type(LOVE).limit(0).summary(1).as(love),reactions.type(HAHA).limit(0).summary(1).as(haha),reactions.type(WOW).limit(0).summary(1).as(wow),reactions.type(SAD).limit(0).summary(1).as(sad),reactions.type(ANGRY).limit(0).summary(1).as(angry),likes.limit(0).summary(true)&limit=1000&access_token=${token}`;
        const url = `https://graph.facebook.com/v9.0/${id}/comments?fields=message,from,id,created_time,updated_time&limit=1000&access_token=${token}`;
        const { data } = await axios({
            url: url,
            method: "get",
            headers: {
                "Content-Type": "application/json",
            },
        });

        res.json(data);
    } catch (error) {
        console.error(error);
        throw error;
    }
});

router.post("/page/:postId/:token", async(req, res) => {
    try {
        const postId = req.params.postId;
        const token = req.params.token;
        console.log(postId);
        console.log(token);
        // const url = `https://graph.facebook.com/v9.0/${postId}/comments?fields=from,message,created_time,id&limit=1000&access_token=${token}`;
        // const { data } = await axios({
        //     url: url,
        //     method: "get",
        //     headers: {
        //         "Content-Type": "application/json",
        //     },
        // });

        // // https://developer.paypal.com/docs/api/orders/v2/#definition-processor_response
        // res.json(data);
    } catch (error) {
        console.error(error);
        throw error;
    }
});
//customer
require("./app/routes/customer.routes.js")(router);
//add the router
app.use("/", router);
app.listen(process.env.port || 3000);

console.log("Running at Port 3000");

// app.listen(3000, function() {
//     console.log("CORS-enabled web server listening on port 3000");
// });